import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anfauske on 25.06.15.
 */
public class Shutdown {
    public static void main(String[] args) throws RuntimeException, IOException, InterruptedException {

        // Systemkommando
        String kommando = "shutdown -h now";

        Scanner sc = new Scanner(System.in);
        System.out.println("Angi tid i timer:");

        // Konverterer timer til ms
        Long timer = Long.parseLong(sc.nextLine());
        Long ms = timer * 3600000;

        System.out.println("Maksinen slås av om " +timer+ " timer.");
        Thread.sleep(ms);

        Runtime.getRuntime().exec(kommando);
        System.exit(0);
    }
}
